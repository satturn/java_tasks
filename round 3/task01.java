import java.util.Scanner;
/* В данной строке найти количество цифр. Вывести количество и сумму цифр.(С клавиатуры) 
Наследование от String*/

public class task01 {
    public static void main (String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter text: ");
        char[] text = input.nextLine().toCharArray();
        input.close();
        
        int count = 0;
        int sum = 0;
        for (Character c : text) {
            if (Character.isDigit(c)) {
                count++;
                sum += Character.getNumericValue(c);
            }
        }
        System.out.println("Count: " + count + ", sum: " + sum);
    }
}