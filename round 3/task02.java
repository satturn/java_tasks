/* Заменить каждый элемент списка с четным номером на соседний слева элемент.
Наследование от ArrayList*/

public class task02 {
    public static void main (String args[]) {
        CustomArrayList<Integer> l = new CustomArrayList<Integer>();
        for(int i = 0; i < 10; i++) {
            l.add(i);
            System.out.print("" + l.get(i) + " ");
        }
        System.out.print("\n");
        l.replaceElems();

        for(int i = 0; i < 10; i++) {
            System.out.print("" + l.get(i) + " ");
        }
    }
}