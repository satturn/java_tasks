import java.util.ArrayList;

class CustomArrayList<T> extends ArrayList<T> {
    public CustomArrayList<T> replaceElems() {
        if (this.size() > 1) {
            for(int i = 1; i < this.size(); i+=2) {
                this.set(i, this.get(i-1));
            }
        }
        return this;
    }
    public CustomArrayList<T> longestStrings() {
        CustomArrayList<T> longest = new CustomArrayList<>();
        this.sort((T e1, T e2) -> e2.toString().length() - e1.toString().length());
        int len = this.get(0).toString().length();
        for (T elem : this) {
            if(elem.toString().length() == len) {
                longest.add(elem);
            }
            else break;
        }
        return longest;
    }
}