import java.util.Scanner;

/* Cоздадим список строк, считаем с клавиатуры 5 штук и добавим их в список. 
Затем с помощью цикла найдем из списка самую длинную строку (или несколько, если она такая не одна).
Cамые длинные строки будут выведены на экран.
Наследование от ArrayList*/

public class task03 {
    public static void main (String args[]) {
        CustomArrayList<String> l = new CustomArrayList<String>();
        Scanner input = new Scanner(System.in);
        System.out.println("Plz enter 5 strings: ");
        for (int i = 0; i < 5; i++) {
            l.add(input.nextLine());
        }
        input.close();

        CustomArrayList<String> longStrings = new CustomArrayList<String>();
        longStrings = l.longestStrings();
        System.out.print("Longest strings: ");
        for(String elem : longStrings) {
            System.out.print("" + elem + " ");
        }
    }
}