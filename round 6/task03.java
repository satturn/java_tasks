import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Считать с консоли данные. Вывести во вторую переменную все числа, которые
 * есть в первом файле. Числа выводить через пробел. Пример - 12 text var2 14 8v
 * 1 Результат: 12 14 1
 */

public class task03 {
    public static void main(String args[]) throws IOException {
        // считали данные, записали в файл
        Scanner input = new Scanner(System.in);
        try (PrintStream out = new PrintStream(new FileOutputStream("task03_output.txt"))) {
            out.println(input.nextLine());
        }
        input.close();

        // открываем файл, считываем данные
        String text = new String(Files.readAllBytes(Paths.get("task03_output.txt")), StandardCharsets.UTF_8);
        String[] vals = text.split(" ");
        ArrayList<Integer> result = new ArrayList<>();
        for (String i : vals) {
            try {
                result.add(Integer.parseInt(i));
            } catch (NumberFormatException e) {
            }
        }
        for(int i : result) {
            System.out.print("" + i + " ");
        }
    }
}