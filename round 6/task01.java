import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Сформируй часть запроса WHERE используя StringBuilder(не обязательно). Если значение null, то параметр не должен попадать в запрос. 
 * Пример: {"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null} 
 * Результат: "name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'" 
 */


public class task01 {
    public static void main (String args[]) {
        Map<String, String> conditions = new HashMap<>();
        conditions.put("name", "Ivanov");
        conditions.put("country", "Ukraine");
        conditions.put("city", "Kiev");
        conditions.put("age", null);

        StringJoiner joiner = new StringJoiner(" and ");

        for (Map.Entry<String, String> item : conditions.entrySet()) {
            if (item.getValue() != null) {
                joiner.add(item.getKey() + " = '" + item.getValue() + "'");
            }
        }

        System.out.println(joiner);
    }
}