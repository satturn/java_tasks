import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.*;
import java.util.stream.Stream;

/**
 * Считать с консоли имя файла. Файл содержит слова, разделенные знаками
 * препинания. Вывести в консоль количество слов "world", которые встречаются в
 * файле. Закрыть потоки.
 */

public class task04 {
    public static void main(String args[]) throws IOException {
        Scanner input = new Scanner(System.in);
        String fileName = input.nextLine();
        input.close();

        String text = new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
        Matcher matcher = Pattern.compile("world").matcher(text);
        int worldCount = Stream.iterate(0, i -> i + 1).filter(i -> !matcher.find()).findFirst().get();
        System.out.println(worldCount);
    }
}