import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 * Считать с консоли данные(массив слов). Записать в другую переменную String
 * строку(через запятую слова), длина которых строго больше 6. В конце запятой
 * не должно быть.
 */

public class task02 {
    public static void main (String args[]) {
        ArrayList<String> words = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        for(int i = 0; i < 5; i++) {
            words.add(input.nextLine());
        }
        input.close();

        StringJoiner joiner = new StringJoiner(",");
        for (String word : words) {
            if (word.length() > 6) {
                joiner.add(word);
            }
        }

        String result = joiner.toString();
        System.out.println(result);
    }
}