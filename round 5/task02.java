import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Ввести с консоли имя файла. Найти байт или байты с максимальным количеством
 * повторов. Вывести их на экран через пробел. Закрыть поток ввода-вывода.
 */

public class task02 {
    public static void main(String args[]) throws IOException {
        Scanner input = new Scanner(System.in);
        String fileName = input.nextLine();
        input.close();
        
        FileInputStream f = new FileInputStream(fileName);
        Map<Character, Integer> dict = new HashMap<Character, Integer>();
        int aval = f.available();
        for(int i = 0; i < aval; i++) {
            char c = (char) f.read();
            if(dict.containsKey(c)) {
                int val = dict.get(c);
                dict.put(c, ++val);
            }
            else {
                dict.put(c, 1);
            }
        }
        f.close();

        dict.entrySet().stream()
        .filter(entry -> entry.getValue() == Collections.max(dict.values()))
        .map(entry -> entry.getKey())
        .collect(Collectors.toList()).forEach(result -> System.out.print(result + " "));
    }
}