import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Написать программу, которая создаст текстовый файл и запишет в него список
 * файлов (путь, имя, дата создания) из заданного каталога.
 */

public class task03 {
    public static void main(String args[]) throws FileNotFoundException {
        //считываем путь
        Scanner input = new Scanner(System.in);
        String folderPath = input.nextLine();
        input.close();

        //записываем все, что есть в каталоге
        File[] filesList = new File(folderPath).listFiles();
        //записываем в файл только информацию о файлах каталога
        try (PrintStream out = new PrintStream(new FileOutputStream("task03_output"))) {
            for (File file : filesList) {
                if (file.isFile()) {
                    out.println((file.getPath() + " " + file.getName() + " " + file.lastModified()));
                }
            }
            out.close();
        }
    }
}