import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Организовать построчное считывание данных, введенных с консоли, и их запись в
 * файл. Признаком окончания записи (конец работы программы) является строка со
 * словом «#ESC».
 */

public class task04 {
    public static void main(String args[]) throws FileNotFoundException {
        Scanner input = new Scanner(System.in);
        try (PrintStream out = new PrintStream(new FileOutputStream("task04_output.txt"))) {
            while (true) {
                String text = input.nextLine();
                if (!text.equals("#ESC")) {
                    out.println(text);
                }
                else {
                    input.close();     
                    out.close();
                    break;
                }
            }
        }
        
    }
}