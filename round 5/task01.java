import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

/**
 * Напишите калькулятор 1 методом. Метод должен принимать 2 цифровых значения и
 * кое-что ещё. Код ваш будет выглядеть примерно так: код**
 * 
 * Вам надо вписать 3 параметра в сигнатуру метода calculate, дописать 1 команду
 * в return и протестировать вызов этого метода в main. Что должен уметь этот
 * метод: складывать, умножать, делить, вычитать, вычислять корень, возводить в
 * степень; возводить в степень сумму аргументов поделенную на первое число +
 * 117; и все любые другие операции, которые сможете придумать.
 * 
 * Что нельзя использовать: if-else, char как указатель операции, switch-case, и
 * все остальное что вам придет в голову.
 * 
 * Что можно использовать: только лямбды.
 */

public class task01 {

    public static void main (String args[]) {
        List<DoubleBinaryOperator> ops = Arrays.asList(
            (x, y) -> (x + y), 
            (x, y) -> (x - y), 
            (x, y) -> x*y, 
            (x, y) -> x/y, 
            (x, y) -> Math.pow(x, y),
            (x, y) -> Math.pow((x+y)/x + 117, y),
            (x, y) -> Math.sqrt(x));
        System.out.println(calculate(5, 6, ops.get(2)));
    }

    public static double calculate(double a, double b, DoubleBinaryOperator op){
        return op.applyAsDouble(a, b);
    }
}