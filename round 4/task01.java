import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/* Узнать день недели и вывести его, если у нас есть дана дата в String формате
 * или timeStamp
*/

public class task01 {

    public static String weekday(String d) {
        LocalDate date = LocalDate.parse(d, DateTimeFormatter.ofPattern("yyyy.MM.dd"));
        return date.getDayOfWeek().toString();
    }

    public static String weekday(Timestamp d) {
        LocalDate date = LocalDate.parse(d.toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.m"));
        return date.getDayOfWeek().toString();
    }

    public static void main(String args[]) {
        String d = "2020.04.04";
        
        DateFormat datef = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
        Date date = new Date();
        try {
            date = datef.parse("2020.01.03 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timestamp ts = new Timestamp(date.getTime());

        System.out.println("String date: " + d + ", weekday: " + weekday(d));
        System.out.println("Timestamp: " + ts + ", weekday: " + weekday(ts));
    }
}