import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/* Напишите программу, которая вводит с клавиатуры номер месяца и день, и определяет, сколько дней осталось до Нового года. 
При вводе неверных данных должно быть выведено сообщение об ошибке. Считается, что год невисокосный.

Входные данные
Входная строка содержит два целых числа: номер месяца и номер дня в этом месяце.
Выходные данные
Программа должна вывести количество дней, оставшихся до Нового года. Если введены неверные данные, нужно вывести число -1.

Примеры
входные данные
1 2                         12 30
выходные данные             
363                         1
 */

public class task03 {
    public static void main (String args[]) {
        Scanner input = new Scanner(System.in);
        try {
            String s = input.nextLine();
            String[] splitStrings = s.split(" ");

            LocalDate date = LocalDate.parse("0001-" + 
            (Integer.parseInt(splitStrings[0]) < 10 ? "0" + splitStrings[0] : splitStrings[0]) + "-" + 
            (Integer.parseInt(splitStrings[1]) < 10 ? "0" + splitStrings[1] : splitStrings[1]));
            System.out.println(365 - date.getDayOfYear());
        }
        catch(NumberFormatException | DateTimeParseException e) {
            System.out.println(-1);
        }
        input.close();
    }
}