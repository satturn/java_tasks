import java.time.LocalDate;

/*  Станция Небула-1 принимает звездолёты только по нечётным дням стандартного земного года (когда количество дней с начала года — нечётно). 
    Выясним, можем ли мы сегодня отдохнуть на Небуле-1. Для этого реализуем метод isDateOdd(String date), возвращающий true, 
    если количество дней с начала года — нечётное число, иначе false. */

public class task02 {
    static boolean isDateOdd(String date) {
        return LocalDate.parse(date).getDayOfYear() % 2 == 0 ? false : true;
    }
    
    public static void main(String args[]) {
        System.out.println(isDateOdd("2020-03-03"));
    }
}