/*Создать иерархию классов, описывающих банковские карточки.
Иерархия должна иметь хотя бы три уровня. */

abstract class BankCard {
    String cardOwner;
    String issuer;
    String cardNumber;
    boolean isLoan;
    boolean otherATMsupport;
}

class LocalCard extends BankCard {
    String localService;
}

class InternationalCard extends BankCard {
    String finService;
}

class CreditCard extends LocalCard {
    double cardLimit;
    
    public CreditCard(String cardOwner, String issuer, String cardNumber, double cardLimit, String localService) {
        this.cardOwner = cardOwner;
        this.issuer = issuer;
        this.cardNumber = cardNumber;
        this.cardLimit = cardLimit;
        this.localService = localService;
    }
}

class DebitCard extends InternationalCard {
    public DebitCard(String cardOwner, String issuer, String cardNumber, String finService) {
        this.cardOwner = cardOwner;
        this.issuer = issuer;
        this.cardNumber = cardNumber;
        this.finService = finService;
    }
}

public class task04 {
    public static void main (String args[]) {
        DebitCard d = new DebitCard("Kek Lolov", "KekBank", "4269420", "Visa");
        CreditCard c = new CreditCard("Ivan Ivanov", "FlexBank", "66632234", 40, "Belcard");

        System.out.printf("Owner: %s\nBank: %s\nCard Number: %s\nService: %s\n\n", d.cardOwner, d.issuer, d.cardNumber, d.finService);
        System.out.printf("Owner: %s\nBank: %s\nCard Number: %s\nCard limit: %f USD\nLocal service: %s\n", c.cardOwner, c.issuer, c.cardNumber, c.cardLimit, c.localService);

    }
}