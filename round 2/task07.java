import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

/*Создать коллекцию, наполнить ее случайными числами. 
Удалить повторяющиеся числа. */

public class task07 {
    public static void main (String args[]) {
        ArrayList<Integer> coll = new ArrayList<>();

        Random rand = new Random();
        for(int i = 0; i < rand.nextInt(100); i++) {
            coll.add(rand.nextInt(11));
            System.out.print(coll.get(i) + " ");
        }
        
        System.out.println();

        coll = (ArrayList<Integer>) coll.stream().distinct().collect(Collectors.toList());

        for(int mark : coll) {
                System.out.print(mark + " ");
        }
    }
}