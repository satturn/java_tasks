import java.util.Scanner;
import java.util.Stack;

/*Создать запрос для вывода только правильно написанных выражений со
скобками (количество открытых и закрытых скобок должно быть одинаково).
– пример правильных выражений: (3*+*5)*–*9*?*4.
– пример неправильных выражений: ((3*+*5)*–*9*?*4. */

public class task02 {
    public static void main (String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter expression: ");
        String expr = input.nextLine();
        input.close();

        Stack<Character> brackets = new Stack<>();
        boolean isValid = true;

        for(char c : expr.toCharArray()) {
            if (c == '(') {
                brackets.push(c);
            }
            if (c == ')') {
                if (!brackets.isEmpty()) {
                    brackets.pop();
                }
                else {
                    isValid = false;
                    break;
                }
            }
        }

        if(brackets.isEmpty() && isValid) {
            System.out.print("Valid expression\n");
        }
        else {
            System.out.print("Invalid expression\n");
        }

    }
}