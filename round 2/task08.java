import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/*Создать список оценок учеников с помощью ArrayList, заполнить случайными
оценками. Найти самую высокую оценку с использованием итератора.  */

public class task08 {
    public static void main (String args[]) {
        ArrayList<Integer> marks = new ArrayList<>();

        Random rand = new Random();
        for(int i = 0; i < rand.nextInt(100); i++) {
            marks.add(rand.nextInt(11));
            System.out.print(marks.get(i) + " ");
        }
        
        System.out.println();

        Iterator<Integer> iter = marks.iterator(); 

        int maxMark = Integer.MIN_VALUE;

        while (iter.hasNext()) {
            int tmp = iter.next();
            if (tmp > maxMark) {
                maxMark = tmp;
            }
        }

        System.out.println("Max mark: " + maxMark);
    }
}