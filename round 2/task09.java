import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/*Напишите метод, который добавляет 1000000 элементов в ArrayList и LinkedList. 
Напишите еще один метод, который выбирает из заполненного списка элемент наугад 10000 раз. 
Замерьте время, которое потрачено на это. Сравните результаты и предположите, 
почему они именно такие.  */

public class task09 {

    public static void addElems(List<Integer> list) {
        Random rand = new Random();
        for (int i = 0; i < 1000000; i++) {
            list.add(rand.nextInt(10));
        }
    }

    public static void getElem(List<Integer> list) {
        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            list.get(rand.nextInt(list.size()));
        }
    }

    public static void main(String args[]) {

        ArrayList<Integer> arrList = new ArrayList<>();
        LinkedList<Integer> linkList = new LinkedList<>();
        addElems(arrList);
        addElems(linkList);

        long startArrTime = System.nanoTime();
        getElem(arrList);
        long endArrTime = System.nanoTime();

        long startLinkedTime = System.nanoTime();
        getElem(linkList);
        long endLinkedTime = System.nanoTime();

        long timeArrElapsed = endArrTime - startArrTime;
        long timeLinkedElapsed = endLinkedTime - startLinkedTime;

        System.out.println("ArrayList: " + timeArrElapsed + " ns");
        System.out.println("LinkedList: " + timeLinkedElapsed + " ns");
    }
}