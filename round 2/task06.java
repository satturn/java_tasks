import java.util.ArrayList;
import java.util.Random;

/*Создать список оценок учеников с помощью ArrayList, заполнить
случайными оценками. Удалить неудовлетворительные оценки из списка.  */

public class task06 {
    public static void main (String args[]) {
        ArrayList<Integer> marks = new ArrayList<>();

        Random rand = new Random();
        for(int i = 0; i < rand.nextInt(100); i++) {
            marks.add(rand.nextInt(11));
            System.out.print(marks.get(i) + " ");
        }
        
        System.out.println();

        marks.removeIf(mark -> mark < 4);

        for(int mark : marks) {
                System.out.print(mark + " ");
        }
    }
}