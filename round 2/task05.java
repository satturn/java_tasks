/*Класс Деньги для работы с денежными суммами. Число должно быть представлено 
двумя полями: типа long для рублей и типа char - для копеек. Дробная часть 
(копейки) при выводе на экран должна быть отделена от целой части запятой. 
Реализовать сложение, вычитание, деление сумм, деление суммы на дробное число, 
умножение на дробное число и операции сравнения. В функции main проверить эти методы. */

class Money implements Comparable {
    long rubles;
    char kopek;

    public Money(long rubles, char kopek) {
        this.rubles = rubles;
        this.kopek = kopek;
    }

    public void print() {
        System.out.println("" + this.rubles + ',' + ((int)this.kopek - '0'));
    }

    public Money sum (long s1r, char s1k, long s2r, char s2k) {
        int new_kopeks = (int) ((s1k - '0') + (s2k - '0'));
        long new_rubles = s1r + s2r;
        if (new_kopeks >= 100) {
            new_kopeks-= 100;
            new_rubles++;
        }
        return new Money(new_rubles, (char)(new_kopeks + '0'));
    }
    //fix it
    public Money sub (long s1r, char s1k, long s2r, char s2k) {
        int new_kopeks = (int) ((s1k - '0') - (s2k - '0'));
        long new_rubles = s1r - s2r;
        if (new_kopeks < 0) {
            new_kopeks = 100 + new_kopeks;
            new_rubles--;
        }
        return new Money(new_rubles, (char)(new_kopeks + '0'));
    }

    public Money div (long s1r, char s1k, long s2r, char s2k) {
        double newRubles = s1r / s2r;
        int integer = (int)newRubles;
        int kopeks = (int) ((newRubles - integer) * 100);
        int newKopeks = (Character.getNumericValue(s1k) / Character.getNumericValue(s2k)) + kopeks;
        if (newKopeks >= 100) {
            newKopeks-= 100;
            newRubles++;
        }
        return new Money((long) newRubles, (char) (newKopeks + '0'));
    }

    public Money div (long s1r, char s1k, double num) {
        double newRubles = s1r / num;
        int integer = (int)newRubles;
        int kopeks = (int) ((newRubles - integer) * 100);
        int newKopeks = (int) (Character.getNumericValue(s1k) / num + kopeks);
        if (newKopeks >= 100) {
            newKopeks-= 100;
            newRubles++;
        }
        return new Money((long) newRubles, (char) (newKopeks + '0'));
    }
    //fix it
    public Money mul (long s1r, char s1k, double num) {
        double newRubles = s1r * num;
        int newKopeks = (int) ((s1k -'0') * num);
        while (newKopeks >= 100) {
            newKopeks-= 100;
            newRubles++;
        }
        return new Money((long) newRubles, (char) (newKopeks + '0'));
    }

    @Override
    public boolean equals(Object o) { 
        if (o == this) { 
            return true; 
        } 
  
        if (!(o instanceof Money)) { 
            return false; 
        } 
        Money c = (Money) o; 
          
        return Double.compare(rubles, c.rubles) == 0
                && Double.compare(kopek, c.kopek) == 0; 
    }
    
    public long compareTo(Money m) {
        long rublesDiff = Long.compare(rubles, m.rubles);
        if (rublesDiff != 0) {
            return rublesDiff;
        }
        return Long.compare(kopek, m.kopek);
    }

    @Override
    public int compareTo(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }
}

public class task05 {
    public static void main (String args[]) {
        Money m = new Money(100, '5');
        m.print();

        Money mul = m.mul(m.rubles, m.kopek, 10.5);
        mul.print();

        Money div = m.div(m.rubles, m.kopek, 2);
        div.print();

        Money div2 = m.div(mul.rubles, mul.kopek, m.rubles, m.kopek);
        div2.print();
        
        Money sum = m.sum(m.rubles, m.kopek, mul.rubles, mul.kopek);
        sum.print();

        Money sub = m.sub(mul.rubles, mul.kopek, m.rubles, m.kopek);
        sub.print();

        System.out.println(m.equals(mul));
        System.out.println(mul.compareTo(m));
    }
}