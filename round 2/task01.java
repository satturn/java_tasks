import java.util.Scanner;

/*Составить регулярное выражение, определяющее является ли 
заданная строка IP адресом, записанным в десятичном виде.
– пример правильных выражений: 127.0.0.1, 255.255.255.0.
– пример неправильных выражений: 1300.6.7.8, abc.def.gha.bcd.  */

public class task01 {
    public static void main (String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter address: ");
        String address = input.nextLine();
        input.close();
        if(address.matches("(\\d{1,3}\\.){3}\\d{1,3}")) {
            System.out.print("Valid address");
        }
        //если принципиально, чтобы были числа в IP до 255, то
        // if(address.matches("((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")) {
        //     System.out.print("Valid address\n");
        // }
        else System.out.print("Invalid address\n");
    }
}