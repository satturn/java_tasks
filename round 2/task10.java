import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*Имеется текст. Следует составить для него частотный словарь */

public class task10 {
    public static void main (String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter text: ");
        String text = input.nextLine();
        input.close();

        String[] words;
        words = text.split(" ");
        Map<String, Integer> dict = new HashMap<String, Integer>();
        for(String word : words) {
            if(dict.containsKey(word)) {
                int val = dict.get(word);
                dict.put(word, ++val);
            }
            else {
                dict.put(word, 1);
            }
        }

        for (Map.Entry<String, Integer> item : dict.entrySet()) {
            System.out.println("word: " + item.getKey() + ", count: " + item.getValue());
        }
        
    }
}