/*Создать иерархию классов, описывающих бытовую технику. Создать
несколько объектов описанных классов, часть из них включить в розетку.
Иерархия должна иметь хотя бы три уровня.  */

abstract class Appliance {
    public void plugIn() {
    }
}

class LargeAppliance extends Appliance {
    int weight;
    int power;
    String connections;
}   

class SmallAppliance extends Appliance {
    boolean isPortable;
}

class Fridge extends LargeAppliance {
    String name;

    public Fridge(String name, int weight, int power, String connections) {
        this.name = name;
        this.power = power;
        this.weight = weight;
        this.connections = connections;
    }

    @Override
    public void plugIn() {
        System.out.println(this + " just plugged in");
    }

    @Override
    public String toString() {
        return name;
        
    }
}

class Toaster extends SmallAppliance {
    String name;
    
    public Toaster(String name, boolean isPortable) {
        this.name = name;
        this.isPortable = isPortable;
    }

    @Override
    public void plugIn() {
        System.out.println(this + " is plugged in");
    }

    @Override
    public String toString() {
        return name;
        
    }
}

public class task03 {
    public static void main (String args[]) {
        Toaster t = new Toaster("lil toast", true);
        Fridge f = new Fridge("fridgee", 90, 220, "electritity");
        Toaster nt = new Toaster("just toast", true);

        t.plugIn();
        f.plugIn();
    }
}