/*Ввести n слов с консоли. Найти слово, символы в котором идут в строгом порядке возрастания их кодов.
Если таких слов несколько, найти первое из них.  */

import java.util.Scanner;
import java.util.ArrayList;

public class task06 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();
        for (String str: strings) {
            boolean isValid = true;
            for(int i = 1; i < str.length(); i++) {
                if (str.charAt(i-1) >= str.charAt(i)) {
                    isValid = false;
                    break;
                }
            }
            if (isValid) {
                System.out.println("Word: " + str);
                break;
            }
        }
    }
}