/*Ввести n строк с консоли. Упорядочить и вывести строки в порядке 
возрастания (убывания) значений их длины. */

import java.util.Scanner;
import java.util.ArrayList;

public class task02 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        //по возрастанию
        strings.sort((String s1, String s2) -> s1.length() - s2.length());
        //по убыванию
        //strings.sort((String s1, String s2) -> s2.length() - s1.length());

        for(int i=0; i<n; ++i) {
            System.out.println(strings.get(i));
        }
    }
}