/*Ввести n слов с консоли. Найти количество слов, содержащих только символы латинского алфавита, 
а среди них – количество слов с равным числом гласных и согласных букв.  */

import java.util.Scanner;
import java.util.ArrayList;

public class task05 {
    public static void main (String args[]) {
        final String vowels = "aeiou";
        ArrayList<String> strings = new ArrayList<>();
        int countLatin = 0;
        int countEqual = 0;

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        for (String str : strings) {
            //if word contains only latin chars
            if(str.matches("[A-Za-z]+")) {
                countLatin++;
      
                int strVowels = 0, strLen = str.length();
                //counting vowels
                for(int i = 0; i < strLen; i++) {
                    char c = str.charAt(i);
                    if (vowels.contains(String.valueOf(c))) {
                        strVowels++;
                    }
                }
                //if number of vowels and consonants are equal
                if ((strLen - strVowels) == strVowels) {
                    countEqual++;
                }
            }
        }

        System.out.printf("Number of latin words: %d\n", countLatin);
        System.out.printf("Number of latin words with equal vowels and consonants: %d\n", countEqual);
    }
}