/*Ввести n слов с консоли. Найти слово, в котором число различных символов минимально. 
Если таких слов несколько, найти первое из них. */

import java.util.Scanner;
import java.util.ArrayList;

public class task04 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();
        Long min = Long.MAX_VALUE;
        int minPos = 0;

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        for (int i = 0; i < strings.size(); i++) {
            String str = strings.get(i);
            long tmp = str.chars().distinct().count();

            if (tmp < min){
                min = tmp;
                minPos = i;
            }
        }

        System.out.printf("Word: %s; number of symbols: %d\n", strings.get(minPos), min);
    }
}