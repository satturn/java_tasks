/* Ввести n слов с консоли. Среди слов, состоящих только из цифр, найти слово-палиндром. 
Если таких слов больше одного, найти второе из них. */

import java.util.Scanner;
import java.util.ArrayList;

public class task08 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        ArrayList<String> output = new ArrayList<>();
        for (String str : strings) {
            //if word contains only latin chars
            if(str.matches("[0-9]+")) {
                int len = str.length();
                boolean isPalindrome = true;
                for (int i = 0; i < len/2; i++) {
                    if (str.charAt(i) != str.charAt(len-i-1)) {
                        isPalindrome = false;
                        break;
                    }
                }
                if (isPalindrome) {
                    output.add(str);
                }
            }
        }
        if (output.size() > 1) 
            System.out.println("Second palindrome: " + output.get(1));
        else 
            System.out.println("Palindrome: " + output.get(0));
    }
}