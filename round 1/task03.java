/*Ввести n строк с консоли. Вывести на консоль те строки, 
длина которых меньше (больше) средней, а также длину. */

import java.util.Scanner;
import java.util.ArrayList;

public class task03 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        //по возрастанию
        strings.sort((String s1, String s2) -> s1.length() - s2.length());

        double avg = (strings.get(n-1).length() + strings.get(0).length())/2.;

        System.out.printf("Average length: %f\nStrings:\n", avg);
        for(String str: strings) {
            if (str.length() < avg) {
                System.out.printf("%s; length: %d\n", str, str.length());
            }
        }
    }
}