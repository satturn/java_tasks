/*Ввести n строк с консоли, найти самую короткую и самую длинную строки. 
Вывести найденные строки и их длину. */

import java.util.Scanner;
import java.util.ArrayList;

public class task01 {
    public static void main (String args[]) {
        int minLength = Integer.MAX_VALUE;
        int maxLength = Integer.MIN_VALUE;
        int minPos = 0;
        int maxPos = 0;

        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();
        
        for(int i=0; i < n; i++) {
            int len = strings.get(i).length();
            if (len > maxLength) {
                maxLength = len;
                maxPos = i;
            }
            if (len < minLength) {
                minLength = len;
                minPos = i;
            }
        }

        System.out.printf("Shortest string: %s, length: %d\n", strings.get(minPos), minLength);
        System.out.printf("Longest string: %s, length: %d\n", strings.get(maxPos), maxLength);
    }
}