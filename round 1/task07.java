/*Ввести n слов с консоли. Найти слово, состоящее только из различных символов.
Если таких слов несколько, найти первое из них.*/

import java.util.Scanner;
import java.util.ArrayList;

public class task07 {
    public static void main (String args[]) {
        ArrayList<String> strings = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Plz enter n: ");
        int n = input.nextInt();
        input.nextLine();

        System.out.println("Plz enter strings: ");
        for(int i=0; i<n; ++i) {
            String tmp = input.nextLine();
            strings.add(tmp);
        }
        input.close();

        for (String str: strings) {
            if(str.chars().distinct().count() == str.length()) {
                System.out.println("Word: " + str);
                break;
            }
        }
    }
}